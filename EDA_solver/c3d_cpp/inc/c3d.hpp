#pragma once

#include <vector>
#include <complex>
#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cmath>

#include <yakutat/DynamicMatrix.hpp>
#include <yakutat/Algorithms/LU/LD.hpp>
#include <yakutat/Algorithms/LinearSolvers/cg.hpp>
#include <yakutat/Algorithms/LinearSolvers/bicgstab.hpp>

#define PI 3.1415926536
#define ZETA3 1.2020569032
#define MAXCON 20
#define MAXSUR  100
#define MAXVAR 2000

using size_type         = size_t;
using parameter_type    = const size_type;
using value_type        = double;
using string_type       = std::string;
using array_string_type = std::vector<string_type>;   //new !! 
using matrix_string_type  = std::vector<std::vector<string_type> >;   //new !! 
using array_int_type   = std::vector<int>;
using array_size_type   = std::vector<size_type>;
using array_value_type  = std::vector<value_type>;
using matrix_value_type = std::vector<std::vector<value_type> >;
using matrix_size_type  = std::vector<std::vector<size_type> >;
using complex_type      = std::complex<value_type>;
using array_complex_type= std::vector<complex_type>;


/*............ function in c3d_solver.cpp file.........*/
bool calculate(void); 

/*............ function in c3d_io.cpp file............*/ 
bool read_in_data_file(void);

/*............ function in c3d_func.cpp file............*/
void  divgen(size_type, size_type, array_value_type&); //DIVGEN(NU,NC,DIV)
size_type ichr(const string_type); //ICHR(C0)
bool input(const size_type&, const array_int_type&, const array_value_type&,array_int_type&,array_value_type& ,
           array_value_type& , array_value_type&, array_size_type&, array_size_type&, array_value_type&); 
bool store(array_value_type&, size_type&, array_value_type&);
bool poke(array_value_type&, array_value_type&, size_type, const array_value_type&);
bool eqcap(array_value_type& , array_value_type& , array_value_type& , size_type);

/*............ c3d_unitfunc.cpp ............*/ 
bool series(value_type &, value_type &, value_type &, value_type &, value_type &);
bool green(value_type &);
bool kernel(value_type &);
value_type averg(value_type, value_type, value_type, value_type, value_type);
bool FFT(array_complex_type &, size_type, int);
bool gels(array_value_type &, array_value_type &, size_type, size_type, value_type, int, array_value_type &);
bool cgels(array_value_type &, array_value_type &, size_type, size_type, value_type, int, array_value_type &);
