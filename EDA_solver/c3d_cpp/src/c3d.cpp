#include "c3d.hpp"
#include "c3d_variables.hpp"




int main(int argc, char *argv[])
{
    // array_value_type R(1920), A(4656), AUX(96);
    // size_type temp, KIND,NTVAR,NC;
    // int IER;
    // value_type AUX_val, R_val, A_val;
    // std::ifstream file;
    // file.open("debug_input_CGELS.DAT");

    // for(size_type i=0; i<96; ++i)
    // {
    //     file >> temp >> AUX_val;
    //     AUX[temp-1] = AUX_val;
        
    // }

    // for(size_type i=0; i<1920; ++i)
    // {
    //     file >> temp >> R_val;
    //     R[temp-1] = R_val;
    // }

    // for(size_type i=0; i<4656; ++i)
    // {
    //     file >> temp >> A_val;
    //     A[temp-1] = A_val;
        
    // }
    // file.close();



    // KIND = 2;
    // NTVAR = 96;
    // NC = 0;
    // std::cout << "EQCAP debug1  " << KIND << "   " << R[0] << "  " 
    //             << "  " << A[0] << "  " << NTVAR << "  "
    //             <<  NC << "  " << IER << "  " << AUX[0] <<
    // std::endl;


    // cgels(R, A, NTVAR, NC, 1.0E-5, IER, AUX);


    // std::cout << "EQCAP debug2  " << KIND << "   " << R[0] << "  " 
    //             << "  " << A[0] << "  " << NTVAR << "  "
    //             <<  NC << "  " << IER << "  " << AUX[0] <<

    
    // std::endl;

    // ===== file name ===== //
    // infile  = "input_files/VIA.DAT";
    // infile  = "input_files/CRO.DAT";
     infile  = "input_files/C3DIN.DAT";
    // ===== file name ===== //
    std::cout << infile << std::endl;
    bool read_status, cal_status;

    read_status = read_in_data_file();

    if (read_status)
    {
        std::cout<< "\n\n>>> start calculate" << std::endl;
        cal_status = calculate();   
        std::cout<< ">>> end calculate" << std::endl;

        if (cal_status)
            std::cout<<"OK!"<<std::endl;
        else
            std::cout<<"ERROR!!!! error in the status of the function calculate() !!!!"<<std::endl;
    }   
    else
    {
        std::cout << "ERROR!!!! error in the status of the function read_in_data_file() !!!!"<<std::endl;
    }

    return 0;
}

