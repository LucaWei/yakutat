#include "c3d.hpp"
#include <sstream>


extern string_type       infile;
// extern array_ineger_type  nsurf, ndiv, ncode;
// extern array_ineger_type  nsym, ncelm;
// extern array_value_type  vop1, vop2, vop0, vinp, dinp, udiv, vdiv;
// extern string_type       c0, cc, cf;
// extern array_string_type ch;

bool read_in_data_file()
{
    // variables
    array_int_type nsurf(MAXCON), ndiv(3), ncode(3);
    array_int_type nsym(3), ncelm(MAXCON);           // 重複變數名 !!!!!! NSYM!!!!
    array_value_type  vop1(3), vop2(3), vop0(3), vinp(3), dinp(3), udiv(99), vdiv(99);
    array_value_type  allvop1, allvop2, allvop0, alludiv_vdiv; // added by katelyn
    array_size_type   allnu, allnv;              // added by katelyn
    string_type       c0, cc, cf;
    array_string_type ch(6);
    string_type ctemp, temp;
    size_type ncond, i;
    value_type rdiel, scale, hp, freq;
    array_value_type input_phase_info(4);


    std::ifstream file1;
    std::cout<< "===> INPUT FILENAME (e.g. C3DIN.DAT) ===> "<<std::endl;
    file1.open(infile); 
    if (!file1.is_open()){std::cout<<"Could not open the file !!!!"<<std::endl; return false;}
    
    std::cout<<"++ ECHO OF INPUT FILE ++ ,(CHECK WITH YOUR INPUT FILE FORMAT, PLEASE)"<<std::endl;
    
    file1 >> temp;
    
    //==============INPUT PHASE==============//

    if(ichr(temp)!= 11 && !temp.empty())
    {
        ncond = std::stoi(temp);
        file1 >>nsym[0]>>nsym[1]>>nsym[2]>>rdiel>>scale>>hp>>freq;

        // print info
        std::cout<<" "<<ncond<<"  "<<nsym[0]<<"  "<<nsym[1]<<"  "<<nsym[2]<<"  "
                 <<std::fixed<< std::setprecision(4)<<rdiel<<"  "
                 <<std::fixed<< std::setprecision(4)<<scale<<"  "
                 <<std::fixed<< std::setprecision(3)<<hp<<"  "
                 <<std::fixed<< std::setprecision(3)<<freq<<"\n";
        
        // for input()
        input_phase_info[0] = rdiel;
        input_phase_info[1] = scale;
        input_phase_info[2] = hp;
        input_phase_info[3] = freq;

    }

    if ( hp <= 0. ) { freq = 0. ;}

    //  WRITE(8) !!!!!!!!!!! GEO.$$$ --> ncond,(nsym(I),I=1,3),rdiel,scale,hp,freq  

    for ( i = 0; i < ncond; ++i )
    {
        file1 >> ncelm[i];

        // print info
        std::cout<<" "<<ncelm[i];
    }

    std::cout<<std::endl;


    //==============DETERMINE # OF SURFACES IN EACH CONDUCTOR GROUP==============//
    size_type ntelm, ntsur, ncc, ic, is, j0, na, nb; 
    value_type da, dr; 
    size_type nl, nlc,nang, sum_nsurf = 0;
    ntelm = 0;

    for ( ic = 0; ic< ncond; ++ic) //80
    {
        ntsur = 0;
        ncc = ncelm[ic];
        ntelm = ntelm + ncc; // 1

        for ( is = 0; is < ncc; ++is )  //70
        {

            file1 >> temp;

            ///THIS IS A RECTANGULAR BOX
            if (ichr(temp) == 5)
            {
                file1 >> ctemp;

                for ( i = 0; i < 6; ++i)
                {
                    if ( ctemp == "F" || ctemp == "P" || ctemp == "R" || ctemp == "L" || ctemp == "T" ||ctemp == "B" )
                    {
                        ch[i] = ctemp;
                        file1 >> ctemp;
                    }
                    else
                    {
                        ch[i] = " ";
                    }
                    
                }

                vinp[0] = std::stod(ctemp);

                file1 >>vinp[1]>>vinp[2]>>dinp[0]>>ndiv[0]>>ncode[0]
                                        >>dinp[1]>>ndiv[1]>>ncode[1]
                                        >>dinp[2]>>ndiv[2]>>ncode[2];


                // somthing important
                ntsur = ntsur + 6;
                for ( i = 0; i < 6; ++i)
                {
                    j0 = ichr(ch[i]);
                    if ( j0 >= 1 && j0 <= 6)
                        ntsur = ntsur - 1;
                }
                
                // print info
                std::cout<<temp<<" "<<ch[0]<<ch[1]<<ch[2]<<ch[3]<<ch[4]<<ch[5]
                << vinp[0]<<" "<<vinp[1]<<" "<<vinp[2]<<" "<<dinp[0]<<" "<<ndiv[0]<<" "<<ncode[0]
                                                      <<" "<<dinp[1]<<" "<<ndiv[1]<<" "<<ncode[1]
                                                      <<" "<<dinp[2]<<" "<<ndiv[2]<<" "<<ncode[2];
                std::cout<<std::endl;
                
                
            } 
            else if (ichr(temp) == 13)
            {
                /// THIS IS A ZERO THICKNESS GENERAL PLANE
                file1 >> vop0[0]>>vop0[1]>>vop0[2]
                      >> vop1[0]>>vop1[1]>>vop1[2]
                      >> ndiv[0]>>ncode[0]
                      >> vop2[0]>>vop2[1]>>vop2[2]
                      >> ndiv[1]>>ncode[1];       
                
                // somthing important
                ntsur = ntsur +1;
                
                // print info
                std::cout<<temp
                         <<" "<<vop0[0]<<" "<<vop0[1]<<" "<<vop0[2]
                         <<" "<<vop1[0]<<" "<<vop1[1]<<" "<<vop1[2]
                         <<" "<<ndiv[0]<<" "<<ncode[0]
                         <<" "<<vop2[0]<<" "<<vop2[1]<<" "<<vop2[2]
                         <<" "<<ndiv[1]<<" "<<ncode[1];    
                std::cout<<std::endl;

            }
            else if (ichr(temp) == 1)
            {
                ///  THIS IS A ZERO THICKNESS PLANE
               file1 >> ctemp;

                for ( i = 0; i < 6; ++i)
                {
                    if ( ctemp == "X" || ctemp == "Y"||ctemp == "Z")
                    {
                        ch[i] = ctemp;
                        file1 >> ctemp;
                    }
                    else
                    {
                        ch[i] = " ";
                    }
                    
                }

                vinp[0] = std::stod(ctemp);

                file1 >>vinp[1]>>vinp[2]>>dinp[0]>>ndiv[0]>>ncode[0]
                                        >>dinp[1]>>ndiv[1]>>ncode[1];

                // somthing important
                na = ichr(ch[0])-6;

                if ( na >= 1 && na <= 3 )
                {
                    //GO TO 45
                    nb = ichr(ch[1])-6;
                    
                    if ( nb>= 1 && nb <= 3)
                    {
                        //GO TO 50
                        if ( nb != na )
                            ntsur = ntsur + 1;
                        else
                            std::cout<<"ERROR IN 4TH OR 5TH COLUMN, THEY SHOULD NOT EQUAL"<<std::endl;
                    }
                    else
                    {
                        //WRITE(6,590)
                        std::cout<<"ERROR CODE IN 5TH COLUMN, IT SHOULD BE X,Y, OR Z"<<std::endl;
                        //GO TO 70
                    }
                }
                else 
                {
                    //WRITE(6,580)
                    std::cout<<"ERROR CODE IN 4TH COLUMN, IT SHOULD BE X,Y, OR Z"<<std::endl;
                    //GO TO 70
                }

                
                // print info
                std::cout<<temp<<" "<<ch[0]<<ch[1]<<ch[2]<<ch[3]<<ch[4]<<ch[5]
                << vinp[0]<<" "<<vinp[1]<<" "<<vinp[2]<<" "<<dinp[0]<<" "<<ndiv[0]<<" "<<ncode[0]
                                                      <<" "<<dinp[1]<<" "<<ndiv[1]<<" "<<ncode[1];
                                                      
                std::cout<<std::endl;  


            }
            else if (ichr(temp) == 10)
            {
                //THIS IS A CIRCULAR BAR
                file1 >> ctemp;

                for ( i = 0; i < 6; ++i)
                {
                    if ( ctemp == "X" || ctemp == "Y"||ctemp == "Z")
                    {
                        ch[i] = ctemp;
                        file1 >> ctemp;
                    }
                    else
                    {
                        ch[i] = " ";
                    }
                    
                }


                vinp[0] = std::stod(ctemp);
                file1 >>vinp[1]>>vinp[2]>>da>>nl>>nlc>>dr>>nang; // should be checked the type of variables


                // somthing important
                na = ichr(ch[0])-6;

                if (na >= 1 && na <= 3 )
                    ntsur = ntsur +nang;
                else
                    std::cout<<"ERROR CODE IN 4TH COLUMN, IT SHOULD BE X,Y, OR Z"<<std::endl;  

            

                // print info
                std::cout<<temp<<" "<<ch[0]<<ch[1]<<ch[2]<<ch[3]<<ch[4]<<ch[5]
                << vinp[0]<<" "<<vinp[1]<<" "<<vinp[2]<<" "<<da<<" "<<nl<<" "<<nlc
                                                               <<" "<<dr<<" "<<nang;
                                                      
                std::cout<<std::endl;

               


            }
            else
            {
                std::cout<<">> ERROR !!! ERROR CODE IN 2ND COLUMN, IT MUST BE C,P,G, OR R"<<std::endl;
            }



        } // 70

        nsurf[ic] = ntsur;
        sum_nsurf = sum_nsurf + ntsur;
        
    } // 80 ( ic = 0; ic< ncond; ++ic) 

    file1.close();

    //  WRITE(8) !!!!!!!!!!! GEO.$$$ -->nsurf 


 //=========================GEOMETRY-GENERATING PHASE=========================//
    size_type iii, i2, i1, nc, nu, nv, nuc, nvc;
    int n1;
    bool if_in;
    size_type iter_5, iter_10, iter_13, iter_1, sum = 0, sum5 = 0, sum10 = 0, sum13 = 0, sum1 = 0, iang;
    value_type radius, sb, sc, angi, angf, dang, fcos, fsin, ang;

    // 要重新讀檔 因為這個case是只有一個group 前面變數會被洗掉 所以才需要重新讀 //記得改這邊 VVVVV
    // 此處要多新增part1 & part2讀檔
    // 以下會讀檔part3的部分 讀ntelm次
    file1.open(infile);
    file1 >> temp;

    if(ichr(temp)!= 11 && !temp.empty())
        file1 >>nsym[0]>>nsym[1]>>nsym[2]>>rdiel>>scale>>hp>>freq;

    for ( i = 0; i < ncond; ++i )
        file1 >> ncelm[i];

    // variables resize
    allvop1.resize(sum_nsurf *3);
    allvop2.resize(sum_nsurf *3);
    allvop0.resize(sum_nsurf *3);
    allnv.reserve(sum_nsurf);
    allnu.reserve(sum_nsurf);
    alludiv_vdiv.reserve(100);

    std::cout<<"\n"<<std::endl;

    for ( iii = 0; iii < ntelm; ++iii ) ///155
    {
        // initialize
        if_in = true;
        file1 >> temp;

        if (ichr(temp) == 5) // " R " //
        {
            ///THIS IS A RECTANGULAR BOX

             file1 >> ctemp;

            for ( i = 0; i < 6; ++i)
            {
                if ( ctemp == "F" || ctemp == "P" || ctemp == "R" || ctemp == "L" || ctemp == "T" ||ctemp == "B" )
                {
                    ch[i] = ctemp;
                    file1 >> ctemp;
                }
                else
                {
                    ch[i] = " ";
                }
                    
            }

            vinp[0] = std::stod(ctemp);

            file1 >>vinp[1]>>vinp[2]>>dinp[0]>>ndiv[0]>>ncode[0]
                                    >>dinp[1]>>ndiv[1]>>ncode[1]
                                    >>dinp[2]>>ndiv[2]>>ncode[2];
            
            // std::cout<<temp<<" "<<ch[0]<<ch[1]<<ch[2]<<ch[3]<<ch[4]<<ch[5]
            //     << vinp[0]<<" "<<vinp[1]<<" "<<vinp[2]<<" "<<dinp[0]<<" "<<ndiv[0]<<" "<<ncode[0]
            //                                           <<" "<<dinp[1]<<" "<<ndiv[1]<<" "<<ncode[1]
            //                                           <<" "<<dinp[2]<<" "<<ndiv[2]<<" "<<ncode[2]<<std::endl;

            // reset iterator                         
            iter_5 = 0;

            for ( i2 = 0; i2 < 6; ++i2 ) //150
            {
                for ( i1 = 0; i1 < 6; ++i1 )
                {
                    if ( i2+1 == ichr(ch[i1]) )
                    {
                        if_in = false;
                        break;
                    }
                    else
                    {
                        if_in = true;
                    }
                }


                if ( if_in )
                {
                    n1 = (i2 - 1+1)/3;
                    na = (i2+1) - n1*3; 
                    nb = na+1-(na/3)*3;
                    nc = nb+1-(nb/3)*3;

                    na = na -1;
                    nb = nb -1;
                    nc = nc -1;


                    vop0[na] = vinp[na] + dinp[na]*n1;
                    vop1[na] = vop0[na];
                    vop2[na] = vop0[na];

                    vop0[nb] = vinp[nb];
                    vop1[nb] = vinp[nb] + dinp[nb];
                    vop2[nb] = vinp[nb];

                    vop0[nc] = vinp[nc];
                    vop1[nc] = vinp[nc];
                    vop2[nc] = vinp[nc] + dinp[nc];

                    nu = ndiv[nb];
                    nv = ndiv[nc];

                    // for input 1
                    for ( i = 0; i<3; ++i)
                    {
                        allvop1[sum+(iter_5*3)+i] = vop1[i];
                        allvop2[sum+(iter_5*3)+i]=  vop2[i];
                        allvop0[sum+(iter_5*3)+i] = vop0[i];

                        sum5 = sum5+1;          
                    }

                    allnu.push_back(nu);
                    allnv.push_back(nv);
                    

                    nuc = ncode[nb];
                    nvc = ncode[nc];

                    divgen(nu,nuc,udiv);
                    divgen(nv,nvc,vdiv);

                    // for input 2
                    for ( i = 0; i < nu; ++i)
                    {
                        alludiv_vdiv.push_back(udiv[i]);
                    }
                        

                    for ( i = 0; i < nv; ++i)
                    {
                        alludiv_vdiv.push_back(vdiv[i]);
                    }
                    
                    iter_5 = iter_5+1;
                 
                }


            } //150


        }
        else if (ichr(temp) == 13) // " G " //
        {
            /// THIS IS A ZERO THICKNESS GENERAL PLANE
            file1 >> vop0[0]>>vop0[1]>>vop0[2]
                  >> vop1[0]>>vop1[1]>>vop1[2]
                  >> ndiv[0]>>ncode[0]
                  >> vop2[0]>>vop2[1]>>vop2[2]
                  >> ndiv[1]>>ncode[1];             
                
            // print info
            // std::cout<<temp
            //          <<" "<<vop0[0]<<" "<<vop0[1]<<" "<<vop0[2]
            //          <<" "<<vop1[0]<<" "<<vop1[1]<<" "<<vop1[2]
            //          <<" "<<ndiv[0]<<" "<<ncode[0]
            //          <<" "<<vop2[0]<<" "<<vop2[1]<<" "<<vop2[2]
            //          <<" "<<ndiv[1]<<" "<<ncode[1];    
            // std::cout<<std::endl;

            nu = ndiv[0];
            nv = ndiv[1];

            // reset iterator                         
            iter_13 = 0;
            //WRITE(8) (VOP1(I),I=1,3),(VOP0(J),J=1,3),(VOP2(K),K=1,3),NU,NV
            // for input 1
            for ( i = 0; i<3; ++i)
            {
                allvop1[sum+(iter_13*3)+i] = vop1[i];
                allvop2[sum+(iter_13*3)+i]=  vop2[i];
                allvop0[sum+(iter_13*3)+i] = vop0[i];

                sum13 = sum13+1;          
            } 

            allnu.push_back(nu);
            allnv.push_back(nv);

            nuc = ncode[0];
            nvc = ncode[1];
            divgen(nu, nuc, udiv);
            divgen(nv, nvc, vdiv);
            
            //WRITE(8) (UDIV(I),I=1,NU),(VDIV(J),J=1,NV)
            // for input 2
            for ( i = 0; i < nu; ++i)
            {
                alludiv_vdiv.push_back(udiv[i]);
            }
                        

            for ( i = 0; i < nv; ++i)
            {
                alludiv_vdiv.push_back(vdiv[i]);
            }
                    
            iter_13 = iter_13+1;


        }
        else if (ichr(temp) == 1) // " P " //
        {
            ///  THIS IS A ZERO THICKNESS PLANE
            file1 >> ctemp;

            for ( i = 0; i < 6; ++i)
            {
                if ( ctemp == "X" || ctemp == "Y"||ctemp == "Z")
                {
                    ch[i] = ctemp;
                    file1 >> ctemp;
                }
                else
                {
                    ch[i] = " ";
                }
                    
            }

            vinp[0] = std::stod(ctemp);

            file1 >>vinp[1]>>vinp[2]>>dinp[0]>>ndiv[0]>>ncode[0]
                                    >>dinp[1]>>ndiv[1]>>ncode[1];

                
            // print info
            // std::cout<<temp<<" "<<ch[0]<<ch[1]<<ch[2]<<ch[3]<<ch[4]<<ch[5]
            // << vinp[0]<<" "<<vinp[1]<<" "<<vinp[2]<<" "<<dinp[0]<<" "<<ndiv[0]<<" "<<ncode[0]
            //                                           <<" "<<dinp[1]<<" "<<ndiv[1]<<" "<<ncode[1];
                                                      
            //  std::cout<<std::endl;  

            // reset iterator                         
            iter_1 = 0;

            na = ichr(ch[0])-6;
            nb = ichr(ch[1])-6;
            
            if ( (na < 1 || na > 3) || (nb < 1 || nb > 3) || (na == nb))
                if_in = false;
            else
                if_in = true;
            
            
            if (if_in)
            {
                na = na - 1;
                nb = nb - 1;

                for (size_type ip = 0;  ip < 3; ++ip)
                {
                    vop0[ip] = vinp[ip];
                    vop1[ip] = vinp[ip];
                    vop2[ip] = vinp[ip];
                }

                vop1[na] = vinp[na] + dinp[0];
                vop2[nb] = vinp[nb] + dinp[1];
                nu = ndiv[0];
                nv = ndiv[1];

                //WRITE(8) (VOP1(I),I=1,3),(VOP0(J),J=1,3),(VOP2(K),K=1,3), NU,NV
                for ( i = 0; i<3; ++i)
                {
                    allvop1[sum+(iter_1*3)+i] = vop1[i];
                    allvop2[sum+(iter_1*3)+i]=  vop2[i];
                    allvop0[sum+(iter_1*3)+i] = vop0[i];
                    sum1 = sum1+1;          
                } 

                allnu.push_back(nu);
                allnv.push_back(nv);


                nuc = ncode[0];
                nvc = ncode[1];
                divgen(nu, nuc, udiv);
                divgen(nv, nvc, vdiv);

                // WRITE(8) (UDIV(I),I=1,NU),(VDIV(J),J=1,NV)
                // for input 2
                for ( i = 0; i < nu; ++i)
                {
                    alludiv_vdiv.push_back(udiv[i]);
                }
                        

                for ( i = 0; i < nv; ++i)
                {
                    alludiv_vdiv.push_back(vdiv[i]);
                }
                    
                iter_1 = iter_1+1;
            }


        }
        else if (ichr(temp) == 10) // " C " //
        {
            //THIS IS A CIRCULAR BAR
            file1 >> ctemp;

            for ( i = 0; i < 6; ++i)
            {
                if ( ctemp == "X" || ctemp == "Y"||ctemp == "Z")
                {
                    ch[i] = ctemp;
                    file1 >> ctemp;
                }
                else
                {
                    ch[i] = " ";
                }
                    
            }


            vinp[0] = std::stod(ctemp);
            file1 >>vinp[1]>>vinp[2]>>da>>nv>>nvc>>radius>>nang; // should be checked the type of variables


            // print info
            // std::cout<<temp<<" "<<ch[0]<<ch[1]<<ch[2]<<ch[3]<<ch[4]<<ch[5]
            // << vinp[0]<<" "<<vinp[1]<<" "<<vinp[2]<<" "<<da<<" "<<nv<<" "<<nvc
            //                                                    <<" "<<radius<<" "<<nang;
                                                      
            // std::cout<<std::endl;

            na = ichr(ch[0])-6;
            
            if ( na < 1 || na > 3)
                if_in = false;
            else
                if_in = true;
            
            if (if_in)
            {
                nb = na+1-(na/3)*3;
                nc = nb+1-(nb/3)*3;
                sb = 1.;
                sc = 1.;

                // std::cout<< na <<"   "<<nb<<"   "<<nc<<std::endl;
                na = na-1;
                nb = nb-1;
                nc = nc-1;

                if ( (nsym[nb] != 0 && vinp[nb] ==0.) || ( nb == 0 && std::abs(vinp[nb] == hp)) )
                {
                    //GO TO 115
                    angi = -PI/2.;
                    angf =  PI/2.;
                    if ( nb == 0 && vinp[nb] == hp ){ sb = -1.;}
                }
                else
                {
                    angi = -PI;
                    angf =  PI;
                }

                if ( (nsym[nc] != 0 && vinp[nc] == 0. ) || ( nc == 0 && std::abs(vinp[nc] == hp)) ) {angi = 0;}                    
                if ( nc == 0 && vinp[nc] == hp ){ sc = -1.;}

                dang = angf - angi;
                vop0[na] = vinp[na];
                vop1[na] = vinp[na];
                vop2[na] = vinp[na] + da;
                
                fcos = int(100000.*std::cos(angi)+0.5)*0.00001;
                fsin = int(100000.*std::sin(angi)+0.5)*0.00001;

                vop1[nb] = vinp[nb] + radius*fcos*sb;
                vop1[nc] = vinp[nc] + radius*fsin*sc;
                 

                // reset iterator                         
                iter_10 = 0;

                for( iang = 0; iang < nang; ++iang )
                {
                    ang = angi + (dang/nang)*(iang+1);
                    fcos = int(100000.*std::cos(ang)+0.5)*0.00001;
                    fsin = int(100000.*std::sin(ang)+0.5)*0.00001;

                    vop0[nb] = vinp[nb] + radius*fcos*sb;
                    vop0[nc] = vinp[nc] + radius*fsin*sc;
                    vop2[nb] = vop0[nb];
                    vop2[nc] = vop0[nc];
                    nu = 1;

                    //WRITE(8) (VOP1(I),I=1,3),(VOP0(J),J=1,3),(VOP2(K),K=1,3),NU,NV
                    // for input 1
                    for ( i = 0; i<3; ++i)
                    {
                        allvop1[sum+(iter_10*3)+i] = vop1[i];
                        allvop2[sum+(iter_10*3)+i]=  vop2[i];
                        allvop0[sum+(iter_10*3)+i] = vop0[i];

                        sum10 = sum10+1;          
                    } 

                    allnu.push_back(nu);
                    allnv.push_back(nv);


                    udiv[0] = 1.00;
                    divgen(nv, nvc, vdiv);

                   
                   alludiv_vdiv.push_back(udiv[0]);      

                   // for input 2
                   for ( i = 0; i < nv; ++i)
                   {
                     alludiv_vdiv.push_back(vdiv[i]);
                   }

                    //WRITE(8) UDIV(1),(VDIV(J),J=1,NV)
                    

                    vop1[nb] = vop0[nb];
                    vop1[nc] = vop0[nc];

                    iter_10 = iter_10+1;
                
                }//125
                

            }// if_in
  

        }
        else
        {
            std::cout<<">> ERROR !!! ERROR CODE IN 2ND COLUMN, IT MUST BE C,P,G, OR R"<<std::endl;
        }


        sum = sum5 + sum10 +sum13 + sum1;

    } //155

    input( ncond, nsym, input_phase_info, nsurf, allvop1, allvop0, allvop2, allnu, allnv, alludiv_vdiv); //想一下放哪?
    file1.close();

    return true;
}
