#include <cmath>
#include <omp.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <mpi.h>

#include "MPI_tools.hpp"
#include "mpi/SparseMatrix/ELL_SparseMatrix.hpp"
#include "Resolution.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::fstream;
using std::ofstream;



yakutat::SparseMatrixELL<double> diagonal_preconditioner
    (
    // ======================================================== //
    MPI_tools P_MPI, int myid, int nproc,
    yakutat::SparseMatrixELL<double> &matA

    // ======================================================== //
    
    )
{
    // ======================================================== //
    yakutat::SparseMatrixELL<double> Dia_pre_mat((nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells), 1, myid, nproc);
    // ======================================================== //

    Dia_pre_mat = matA.diagonal();
    
    #pragma omp parallel for schedule(guided) 
    for(int i=P_MPI.start; i<P_MPI.end+1; ++i)
        Dia_pre_mat.set(i,i, 1.0/Dia_pre_mat.at(i,i));

    return Dia_pre_mat;
}