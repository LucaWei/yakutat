#include <iostream>
#include <cmath>
#include <omp.h>
#include <vector>
#include <algorithm>
#include "Resolution.hpp"
#include "MPI_tools.hpp"

using std::cout;
using std::endl;
using std::vector;

void DiscretisationLUD
    (
    // ======================================================== //
    MPI_tools &U_MPI,
    size_t num_threads,
    double dt, double dRe,

    vector<vector<double> > &U,
    vector<vector<double> > &U_star,
    vector<vector<double> > &NEIBcell,

    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs

    
    // ======================================================== //
    )
{
    // ======================================================== //
    double Fe, Fw, Fn, Fs, Ff, Fb;


    double phi_e, phi_w, phi_n, phi_s, phi_f, phi_b;
    
    double Fe_phi_e, Fw_phi_w, Fn_phi_n, Fs_phi_s, Ff_phi_f, Fb_phi_b;

    int    xmm, xm, xp, xpp;

    int    ymm, ym, yp, ypp;

    int    zmm, zm, zp, zpp;

    int    icel;

    int omp_count{0};

    // ======================================================== //
    
    // omp_count = (ny-2*gCells)*(nx-2*gCells) / num_threads;
    #pragma omp parallel
    {
    
    for(size_t direction=0; direction<3; ++direction)
    {
    
        for(size_t i = U_MPI.start; i < U_MPI.end+1; ++i )
        {
            #pragma omp for collapse(2) 
            for(size_t j = gCells; j < ny-gCells; ++j )
            {
                for(size_t k = gCells; k < nz-gCells; ++k)
                {
                    icel = i*nz*ny + j*nz + k;
                    xmm = NEIBcell[6][icel];
                    xm  = NEIBcell[0][icel];
                    xp  = NEIBcell[1][icel];
                    xpp = NEIBcell[7][icel]; 

                    ymm = NEIBcell[8][icel];
                    ym  = NEIBcell[2][icel];
                    yp  = NEIBcell[3][icel];
                    ypp = NEIBcell[9][icel];

                    zmm = NEIBcell[10][icel];
                    zm  = NEIBcell[4][icel];
                    zp  = NEIBcell[5][icel];
                    zpp = NEIBcell[11][icel];

                    Fe = 0.5 * (U[0][xp] + U[0][icel]); 
                    Fw = 0.5 * (U[0][xm] + U[0][icel]);
                    Fn = 0.5 * (U[1][yp] + U[1][icel]);
                    Fs = 0.5 * (U[1][ym] + U[1][icel]);
                    Ff = 0.5 * (U[2][zp] + U[2][icel]);
                    Fb = 0.5 * (U[2][zm] + U[2][icel]);

                    

                    Fe_phi_e = (U[direction][icel] + 0.5 * iDx[i] * (U[direction][icel] - U[direction][xm]) / Dxs[i-1]) * std::max(Fe, 0.0) - \
                               (U[direction][xp]   + 0.5 * iDx[i+1] * (U[direction][xp] - U[direction][xpp]) / Dxs[i+1]) * std::max(-Fe,0.0);

                    Fw_phi_w = (U[direction][xm]   + 0.5 * iDx[i-1] * (U[direction][xm] - U[direction][xmm])/ Dxs[i-2]) * std::max(Fw, 0.0) - \
                               (U[direction][icel] + 0.5 * iDx[i] * (U[direction][icel] - U[direction][xp]) / Dxs[i])   * std::max(-Fw,0.0);


                    Fn_phi_n = (U[direction][icel] + 0.5 * iDy[j] * (U[direction][icel] - U[direction][ym]) / Dys[j-1]) * std::max(Fn, 0.0) - \
                               (U[direction][yp]   + 0.5 * iDy[j+1] * (U[direction][yp] - U[direction][ypp])/ Dys[j+1]) * std::max(-Fn,0.0);

                    Fs_phi_s = (U[direction][ym]   + 0.5 * iDy[j-1] * (U[direction][ym] - U[direction][ymm])/ Dys[j-2]) * std::max(Fs, 0.0) - \
                               (U[direction][icel] + 0.5 * iDy[j] * (U[direction][icel] - U[direction][yp]) / Dys[j])   * std::max(-Fs,0.0);


                    Ff_phi_f = (U[direction][icel] + 0.5 * iDz[k] * (U[direction][icel] - U[direction][zm]) / Dzs[k-1]) * std::max(Ff, 0.0) - \
                               (U[direction][zp]   + 0.5 * iDz[k+1] * (U[direction][zp] - U[direction][zpp]) / Dzs[k+1])* std::max(-Ff,0.0);

                    Fb_phi_b = (U[direction][zm]   + 0.5 * iDz[k-1] * (U[direction][zm] - U[direction][zmm]) / Dzs[k-2])* std::max(Fb, 0.0) - \
                               (U[direction][icel] + 0.5 * iDz[k] * (U[direction][icel] - U[direction][zp]) / Dzs[k])   * std::max(-Fb,0.0);


                    // if(Fe>0)
                    // {
                    //     Fe_phi_e = (U[direction][icel] + 0.5 * iDx[i] * (U[direction][icel] - U[direction][xm]) / Dxs[i-1]) * Fe;
                    // }
                    // else
                    // {
                    //     Fe_phi_e = (U[direction][xp]   + 0.5 * iDx[i+1] * (U[direction][xp] - U[direction][xpp]) / Dxs[i+1]) * Fe;
                    // }

                    // if(Fw>0)
                    // {
                    //     Fw_phi_w = (U[direction][xm]   + 0.5 * iDx[i-1] * (U[direction][xm] - U[direction][xmm])/ Dxs[i-2]) * Fw;
                    // }
                    // else
                    // {
                    //     Fw_phi_w = (U[direction][icel] + 0.5 * iDx[i] * (U[direction][icel] - U[direction][xp]) / Dxs[i])   * Fw;
                    // }

                    // if(Fn>0)
                    // {
                    //     Fn_phi_n = (U[direction][icel] + 0.5 * iDy[j] * (U[direction][icel] - U[direction][ym]) / Dys[j-1]) * Fn;
                               
                    // }
                    // else
                    // {
                    //     Fn_phi_n = (U[direction][yp]   + 0.5 * iDy[j+1] * (U[direction][yp] - U[direction][ypp])/ Dys[j+1]) * Fn;
                    // }

                    // if(Fs>0)
                    // {
                    //     Fs_phi_s = (U[direction][ym]   + 0.5 * iDy[j-1] * (U[direction][ym] - U[direction][ymm])/ Dys[j-2]) * Fs;
                    // }
                    // else
                    // {
                    //     Fs_phi_s = (U[direction][icel] + 0.5 * iDy[j] * (U[direction][icel] - U[direction][yp]) / Dys[j])   * Fs;
                    // }

                    // if(Ff>0)
                    // {
                    //     Ff_phi_f = (U[direction][icel] + 0.5 * iDz[k] * (U[direction][icel] - U[direction][zm]) / Dzs[k-1]) * Ff;
                               
                    // }
                    // else
                    // {
                    //     Ff_phi_f = (U[direction][zp]   + 0.5 * iDz[k+1] * (U[direction][zp] - U[direction][zpp]) / Dzs[k+1]) * Ff;

                    // }

                    // if(Fb>0)
                    // {
                    //     Fb_phi_b = (U[direction][zm]   + 0.5 * iDz[k-1] * (U[direction][zm] - U[direction][zmm]) / Dzs[k-2])* Fb;
                               
                    // }
                    // else
                    // {
                    //     Fb_phi_b = (U[direction][icel] + 0.5 * iDz[k] * (U[direction][icel] - U[direction][zp]) / Dzs[k])   * Fb;
                    // }


                    U_star[direction][icel] = U[direction][icel]-dt*(Fe_phi_e-Fw_phi_w) / iDx[i] \
                                                                -dt*(Fn_phi_n-Fs_phi_s) / iDy[j] \
                                                                -dt*(Ff_phi_f-Fb_phi_b) / iDz[k] \

                    +(dRe)*dt*( (U[direction][xp]-U[direction][icel]) / Dxs[i] - (U[direction][icel]-U[direction][xm]) / Dxs[i-1] ) / iDx[i] \
                    +(dRe)*dt*( (U[direction][yp]-U[direction][icel]) / Dys[j] - (U[direction][icel]-U[direction][ym]) / Dys[j-1] ) / iDy[j] \
                    +(dRe)*dt*( (U[direction][zp]-U[direction][icel]) / Dzs[k] - (U[direction][icel]-U[direction][zm]) / Dzs[k-1] ) / iDz[k] ;

        
                }
            }
        }
    } // end of direction

    }













}