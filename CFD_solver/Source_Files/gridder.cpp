#include <omp.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "Resolution.hpp"

using std::cout;
using std::endl;
using std::string;
using std::vector;


void gridder
    (
    // ======================================================== //
    double GridderXc, double GridderYc, double GridderZc, 
    double lxSml, double lySml, double lzSml, double dx,
    double dy, double dz, double dxSml, double dySml, double dzSml,
    string Grid_switch, int myid,

    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs    

    // ======================================================== //
    )


{
    // ======================================================== //

    //Initial grid coordinates for evaluating grid lengths
    vector<double> X(nx+1);
    vector<double> Y(ny+1);
    vector<double> Z(nz+1);


    //Actual grid cooridinates (with adjusted index)
    vector<double> Xa(nx-3);
    vector<double> Ya(ny-3);
    vector<double> Za(nz-3);


    //Midpoints of grid coordinate
    vector<double> Xs(nx-4);
    vector<double> Ys(ny-4);
    vector<double> Zs(nz-4);



    const double xSBgn = GridderXc - lxSml/2.0;

    const double xSEnd = GridderXc + lxSml/2.0;

    const double ySBgn = GridderYc - lySml/2.0;

    const double ySEnd = GridderYc + lySml/2.0;

    const double zSBgn = GridderZc - lzSml/2.0;

    const double zSEnd = GridderZc + lzSml/2.0;


    double xNextLrgValue;

    double xNextSmlValue;
    
    double yNextLrgValue;
    
    double yNextSmlValue;
    
    double zNextLrgValue;
    
    double zNextSmlValue;


    int Nblock = 1;

	int tempNX = nx-gCells*2;

	int tempNY = ny-gCells*2;

	int tempNZ = nz-gCells*2;

	int N_total = tempNX*tempNY*tempNZ;

    vector<vector<vector<float> > > Xout (nx-gCells*2,vector<vector<float> >(ny-gCells*2,vector<float> (nz-gCells*2)) );
    vector<vector<vector<float> > > Yout (nx-gCells*2,vector<vector<float> >(ny-gCells*2,vector<float> (nz-gCells*2)) );
    vector<vector<vector<float> > > Zout (nx-gCells*2,vector<vector<float> >(ny-gCells*2,vector<float> (nz-gCells*2)) );


    // ======================================================== //



    if(Grid_switch == "non-uniform")
    {

        //Unequal grid intervals
        // ======================================================== //
        for(size_t i = 2; i < nx; ++i)
        {
            xNextLrgValue =  X[i-1] + dx;   
            xNextSmlValue =  X[i-1] + dxSml;  

            if(i == 2)
            {
                X[i] = 0.0;
                X[i-1] = X[i] - dx;
                X[i-2] = X[i-1] - dx;

            }
            else if(xNextLrgValue > xSBgn)
            {

                if(xNextSmlValue > xSEnd)
                {

                    X[i] = xNextLrgValue;


                }
                else{

                    X[i] = xNextSmlValue;

                }
            }
            else
            {
                X[i] = xNextLrgValue;
            }
            

        }

        for(size_t j = 2; j < ny; ++j)
        {
            yNextLrgValue =  Y[j-1] + dy;   
            yNextSmlValue =  Y[j-1] + dySml;  

            if(j == 2)
            {
                Y[j] = 0.0;
                Y[j-1] = Y[j] - dy;
                Y[j-2] = Y[j-1] - dy;

            }
            else if(yNextLrgValue > ySBgn)
            {

                if(yNextSmlValue > ySEnd)
                {

                    Y[j] = yNextLrgValue;

                }
                else{

                    Y[j] = yNextSmlValue;

                }

                

            }
            else
            {
                Y[j] = yNextLrgValue;
            }


        }


        for(size_t k = 2; k < nz; ++k)
        {
            zNextLrgValue =  Z[k-1] + dz;   
            zNextSmlValue =  Z[k-1] + dzSml;  

            if(k == 2)
            {
                Z[k] = 0.0;
                Z[k-1] = Z[k] - dz;
                Z[k-2] = Z[k-1] - dz;

            }
            else if(zNextLrgValue > zSBgn)
            {

                if(zNextSmlValue > zSEnd)
                {

                    Z[k] = zNextLrgValue;

                }
                else{

                    Z[k] = zNextSmlValue;

                }

                

            }
            else
            {
                Z[k] = zNextLrgValue;
            }


        }

        // for(size_t i = 2; i < nx; ++i)
        // {
        //     if(i==2)
        //     {
        //         X[i] = 0.0;
        //         X[i-1] = X[i] - dx;
        //         X[i-2] = X[i-1] - dx;
        //     }
        //     else{
        //         X[i] = X[i-1] + dx;
        //     }
        // }


        // for(size_t j = 2; j < ny; ++j)
        // {
        //     if(j==2)
        //     {
        //         Y[j] = 0.0;
        //         Y[j-1] = Y[j] - dy;
        //         Y[j-2] = Y[j-1] - dy;
        //     }
        //     else
        //     {
        //         Y[j] = Y[j-1] + dy;
        //     }
        // }
        
        // for(size_t k = 2; k < nz; ++k)
        // {
        //     if(k==2)
        //     {
        //         Z[k] = 0.0;
        //         Z[k-1] = Z[k] - dz;
        //         Z[k-2] = Z[k-1] - dz;
        //     }
        //     else
        //     {
        //         Z[k] = Z[k-1] + dz;
        //     }
        // }

        
        // ======================================================== //


    }
    else if(Grid_switch == "uniform")
    {

        //equal grid intervals
        // ======================================================== //
        for(size_t i = 2; i < nx; ++i)
        {
            if(i==2)
            {
                X[i] = 0.0;
                X[i-1] = X[i] - dx;
                X[i-2] = X[i-1] - dx;
            }
            else{
                X[i] = X[i-1] + dx;
            }
        }


        for(size_t j = 2; j < ny; ++j)
        {
            if(j==2)
            {
                Y[j] = 0.0;
                Y[j-1] = Y[j] - dy;
                Y[j-2] = Y[j-1] - dy;
            }
            else
            {
                Y[j] = Y[j-1] + dy;
            }
        }
        
        for(size_t k = 2; k < nz; ++k)
        {
            if(k==2)
            {
                Z[k] = 0.0;
                Z[k-1] = Z[k] - dz;
                Z[k-2] = Z[k-1] - dz;
            }
            else
            {
                Z[k] = Z[k-1] + dz;
            }
        }
        // ======================================================== //
    }

    



    




    // Define each of the directional grid lengths
    for (size_t i = 2; i < nx-3; ++i)
    {
        iDx[i] = ( X[i+1] - X[i] );
        Dxs[i] = ( X[i+2] - X[i] ) / 2.0;
    }

    for (size_t j = 2; j < ny-3; ++j)
    {
        iDy[j] = ( Y[j+1] - Y[j] );
        Dys[j] = ( Y[j+2] - Y[j] ) / 2.0;
    }


    for (size_t k = 2; k < nz-3; ++k)
    {
        iDz[k] = ( Z[k+1] - Z[k] );
        Dzs[k] = ( Z[k+2] - Z[k] ) / 2.0;
    }


    // Ghost boundary grid lengths
    iDx[1]    = iDx[2];
    iDx[0]    = iDx[2];
    iDx[nx-3] = X  [nx-2] - X[nx-3];
    iDx[nx-2] = iDx[nx-3];
    iDx[nx-1] = iDx[nx-3];

    Dxs[1]    = Dxs[2];
    Dxs[0]    = Dxs[2];
    Dxs[nx-3] = Dxs[nx-4];
    Dxs[nx-2] = Dxs[nx-4];
    Dxs[nx-1] = Dxs[nx-4];

    iDy[1]    = iDy[2];
    iDy[0]    = iDy[2];
    iDy[ny-3] = Y [ny-2] - Y[ny-3];
    iDy[ny-2] = iDy[ny-3];
    iDy[ny-1] = iDy[ny-3];

    Dys[1]    = Dys[2];
    Dys[0]    = Dys[2];
    Dys[ny-3] = Dys[ny-4];
    Dys[ny-2] = Dys[ny-4];
    Dys[ny-1] = Dys[ny-4];

    iDz [1]    = iDz[2];
    iDz [0]    = iDz[2];
    iDz [nz-3] = Z  [nz-2] - Z[nz-3];
    iDz [nz-2] = iDz[nz-3];
    iDz [nz-1] = iDz[nz-3];

    Dzs[1]    = Dzs[2];
    Dzs[0]    = Dzs[2];
    Dzs[nz-3] = Dzs[nz-4];
    Dzs[nz-2] = Dzs[nz-4];
    Dzs[nz-1] = Dzs[nz-4];



    // Modifying the index of X, Y and Z arrays to represent the actual grid
    for (size_t i = 0; i < nx-3; ++i)
    {
        Xa[i] = X[i+2];
    }

    for (size_t j = 0; j < ny-3; ++j)
    {
        Ya[j] = Y[j+2];
    }

    for (size_t k = 0; k < nz-3; ++k)
    {
        Za[k] = Z[k+2];
    }



    // Defining the midpoint values of the grids
    for (size_t i = 0; i < nx-4; ++i)
    {
        Xs[i] = ( Xa[i] + Xa[i+1] ) / 2.0;
    }

    for (size_t j = 0; j < ny-4; ++j)
    {
        Ys[j] = ( Ya[j] + Ya[j+1] ) / 2.0;
        
    }

    for (size_t k = 0; k < nz-4; ++k)
    {
        Zs[k] = ( Za[k] + Za[k+1] ) / 2.0;

    }

    
    // std::ofstream file("debug_mesh_data.dat",std::ofstream::app);

    // file << endl;
    // file << "distance" << endl;
    // for(size_t i=0; i<nx; ++i)
    // {
    //     file << i  << "       " << iDx[i] << "  " << Dxs[i] << "      " \
    //                << "       " << iDy[i] << "  " << Dys[i] << "      "\
    //                << "       " << iDz[i] << "  " << Dzs[i] << endl;
    // }

    // file << "node" << endl;
    // for(size_t i=0; i<nx+1; ++i)
    // {
    //     file << i << "      " << X[i]  \
    //               << "      " << Y[i]  \
    //               << "      " << Z[i] << endl;
    // }

  
    
    // file.close();






	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				Xout[i][j][k] = Xs[i];
				Yout[i][j][k] = Ys[j];
				Zout[i][j][k] = Zs[k];

			}
		}
	}

    if(myid == 0)
    {
        char LESdata[100];
        FILE *fptr;
        fptr = fopen("Output/P3D.x","wb");
        fwrite(&Nblock, sizeof(int), 1,fptr);

        fwrite(&tempNX, sizeof(int), 1,fptr);
        fwrite(&tempNY, sizeof(int), 1,fptr);
        fwrite(&tempNZ, sizeof(int), 1,fptr);

        for (size_t k = 0; k < nz-gCells*2; ++k) 
        {
            for (size_t j = 0; j < ny-gCells*2; ++j) 
            { 
                for (size_t i = 0; i < nx-gCells*2; ++i) 
                {

                    fwrite(&Xout[i][j][k],sizeof(float),1,fptr);

                }
            }
        }

        for (size_t k = 0; k < nz-gCells*2; ++k) 
        {
            for (size_t j = 0; j < ny-gCells*2; ++j) 
            { 
                for (size_t i = 0; i < nx-gCells*2; ++i) 
                {

                    fwrite(&Yout[i][j][k],sizeof(float),1,fptr);

                }
            }
        }

        for (size_t k = 0; k < nz-gCells*2; ++k) 
        {
            for (size_t j = 0; j < ny-gCells*2; ++j) 
            { 
                for (size_t i = 0; i < nx-gCells*2; ++i) 
                {

                    fwrite(&Zout[i][j][k],sizeof(float),1,fptr);

                }
            }
        }


        
        fclose(fptr);
    }

    



}

