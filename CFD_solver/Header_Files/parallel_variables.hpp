#ifndef _PAPALLEL_VARIABLES_
#define _PAPALLEL_VARIABLES_



    // MPI variables
    // ======================================================== //
    int myid, itag, istart, iend, icount, nproc, lnbr, rnbr, islice;
    const int master = 0;
    double clock, clock1, clock2;
    // ======================================================== //

    // OMP variables
    // ======================================================== //
    size_t num_threads;
    // ======================================================== //

    
#endif