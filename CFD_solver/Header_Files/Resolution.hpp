#ifndef _RESOLUTiON_
#define _RESOLUTiON_

    const int    gCells = 2;

    const int    nx  = 100 + 2*gCells;
          
    const int    ny  = 100 + 2*gCells;
          
    const int    nz  = 100 + 2*gCells;

    const int    iceltot = nx * ny * nz;

    const double lx  = 1;
          
    const double ly  = 1;
          
    const double lz  = 1;


    //Unequal grid
    // ======================================================== //

    const double GridderXc = 0.5;

    const double GridderYc = 0.5;

    const double GridderZc = 0.5;

    const int nxSml = 12;

    const int nySml = 12;

    const int nzSml = 12;

    const double lxSml = 0.1;

    const double lySml = 0.1;

    const double lzSml = 0.1;

    // ======================================================== //


    

#endif